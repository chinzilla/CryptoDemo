﻿using System;                           // Required for the IDisposable interface
using System.Text;                      // Required for the Encoding class
using System.Security.Cryptography;     // Required for the CNG APIs


public partial class CNG_SecureCommunicationExample
{
    // Global variables
    static int MyColor = 4;             // Alice Green displays green text
    static int OtherColor = 2;             // Bob White displays white text

    static void Main()
    {
        if (!Autoloader())                  // Load Bob and Mallory
            return;                         // An error was encountered during load.

        string s = "";
        InitConsole("Alice Green", 5, 5);
        while (true)
        {
            SplashScreen();                 // Set and display titles
            s = InitializeOptions();        // Get session options
            AppControl("send", s);          // Synchronize Bob and Mallory
            if ("exit" == s)                // Quit if user entered "x"
                break;
            Run();                          // Run the desired session
        }
    }                                   // End Main


    //---------------------------------------------------------------------------------------

    static void Run()
    {
        string NewChannelName = "AliceAndBobChannel";
        SendChannelName(NewChannelName);    // Bad corporate security policy 

        Display("Hi, I'm Alice Green. My sales associate is Bob White.\n" +
                "I need to send him a customer order right now!\n\n");

        using (Communicator Alice = new Communicator("server", NewChannelName))
        {
            string s;

            CngKeyCreationParameters keyCreateParms = new CngKeyCreationParameters();
            keyCreateParms.ExportPolicy = CngExportPolicies.AllowPlaintextExport;

            if (3 <= Version)              // Send public digital signature key
            {
                using (CngKey DSKey = CngKey.Create(CngAlgorithm.ECDsaP521, null, keyCreateParms))
                {
                    byte[] dsKeyBlob = DSKey.Export(CngKeyBlobFormat.Pkcs8PrivateBlob);
                    Alice.StoreDSKey(dsKeyBlob);
                    s = Encoding.ASCII.GetString(dsKeyBlob);
                    Display("\nFirst, I will send Bob a digital signature key "
                            + "over a public channel.\n" +
                            (fVerbose ? "Here it is:\n\n" + s + "\n\n" : ""));
                    Alice.ChMgr.SendMessage(dsKeyBlob);
                }
            }

            if (4 <= Version)              // Send private digital signature key
            {
                using (CngKey DSKey = CngKey.Create(CngAlgorithm.ECDsaP521, null, keyCreateParms))
                using (ChannelManager ChMgr2 = new ChannelManager("server", "PrivateChannel"))
                {
                    byte[] dsKeyBlob = DSKey.Export(CngKeyBlobFormat.Pkcs8PrivateBlob);
                    Alice.StoreDSKey(dsKeyBlob);
                    s = Encoding.ASCII.GetString(dsKeyBlob);
                    Display("\nNow I will send Bob a secret digital signature key " +
                            "over a private channel.\n" +
                            (fVerbose ? "Here it is:\n\n" + s + "\n\n" : ""));
                    ChMgr2.SendMessage(dsKeyBlob);
                }
            }

            if (2 <= Version)              // Send and receive public cryptographic keys
            {
                Display(sep2, 1);
                Display("Now we will exchange our public cryptographic\n" +
                        "keys through a public channel\n" +
                        "First, I'll send Bob my key.\n\nSending...\n");
                Alice.Send_or_Receive_PublicCryptoKey("send", MyColor);


                Display(sep2, 1);
                Display("Now Bob will publically send me his " +
                        "public crypto key:\n\nListening...\n");
                if (!Alice.Send_or_Receive_PublicCryptoKey("receive", OtherColor))
                {
                    Display("", 6);
                    return;
                }
            }

            //-----------------------------------------------------------------------------------
            // Now that all the keys have been transmitted, have a conversation.

            if (1 < Version)
            {
                Display(sep2, 1);
                Display("Now that our keys have been exchanged,\n" +
                       "we can have an encrypted conversation:\n\n", 4);
                Display(sep1, 1);
            }

            Alice.SendMessage("Hi Bob. I have a new customer contact.", true);
            Alice.ReceiveMessage();
            Alice.SendMessage("Coho Winery, 111 AnyStreet, Chicago", true);
            Alice.ReceiveMessage();

            if (Version <= 2)
                Display(sep1, 1);

            if (!fVerbose && Version >= 3)
                Display(sep1, 1);

            //-----------------------------------------------------------------------------------
            Display("Would you like to talk to Bob?\n" +
                    "If so, type a message, and press Enter.\n" +
                   "Otherwise, just press Enter.\n\n", 1);
            Display(sep + sep1, 1);
            s = " ";


            while (true)
            {
                s = ReadALine(true);
                if (!Alice.SendMessage(s, false))    // If Bob entered CTRL-C, or SYS_CLOSE
                    break;
                if ("" == s)                        // If user entered ""
                    break;
                s = Alice.ReceiveMessage();         // Read a message
                if ("" == s)
                    break;
            }

            //-----------------------------------------------------------------------------------
        }   // End using ChannelManager, End using Communicator
    }       // End Run

}       // End Alice.cs: public partial class CNG_SecureCommunicationExample
